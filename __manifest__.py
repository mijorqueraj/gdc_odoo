# -*- coding: utf-8 -*-
{
    'name': "gestorIT",

    'summary': """
        Gestion de Cambios en la infraestructura TI""",

    'description': """
        Proyecto de gestionar cambios IT: 
        Permite realizar cambios de infraestructura ti a traves del marco de trabajo COBIT usando ITIL
    """,

    'author': "Miguel Jorquera",
    'website': "http://www.galilea.cl",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/activo.xml',
        'views/orden.xml',
        'views/rfc.xml', 
        'views/respaldo.xml',
        'views/evaluacion.xml',
        'views/tarea.xml'   
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
