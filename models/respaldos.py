#-*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import date

class Respaldo(models.Model):
    _name = 'gestor.respaldos'
    _description = "modulo que permite respaldar la configuracion de activos que quieres ser cambiados"

    configuracion_anterior =  fields.Html(string="Descripcion")
    
    nombre = fields.Char(string="Nombre Cambio", required=True)
    fecha_ultima_configuracion = fields.Date(string="Fecha Ultima configuracion")
    archivos_ids = fields.Many2many('ir.attachment',string="Subir Archivos")
 

    #relaciones con otros modelos
    orden_solicitud_id = fields.Many2one('gestor.orden',ondelete="cascade", string="Orden")
    activo_ids = fields.Many2many('gestor.activo',string="Activos o Artefactos involucrados en el cambio")


    def ver_activo(self):
        objet  =self.env['gestor.orden'].search(['orden_id','=',self.orden_solicitud_id])