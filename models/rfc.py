#-*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import date


class RFC(models.Model):
    
    _name = "gestor.rfc"
    _description = "Clase que permite la gestion de un RFC viene dado de una solicitud de orden para el cambio"

    descripcion = fields.Text(string="Descripcion")
    prioridad = fields.Selection([('alta', 'Alta'),('normal','Normal'),('baja','Baja'),('urgente','Urgente')],string="Prioriad")
    tipo_cambio  = fields.Selection([('estandar','Estándar'),('emergencia','Emergencia')],string="Tipo de Cambio")
    fecha_inicio = fields.Date(default=date.today(),string="Fecha de Inicio Cambio")
    fecha_cierre = fields.Date(default=date.today(),string="Fecha de Termino Cambio")
    tiempo_estimado = fields.Char(readonly=True,string="Tiempo Estimado de Cambio")

    orden_id = fields.Many2one('gestor.orden',string="Orden",ondelete="restrict",required=True)
    activo_ids = fields.Many2many('gestor.activo','gestor_activo_rel',string="Activos o Artefactos involucrados en el cambio")

    recurso_ids = fields.Many2many('res.users','gestor_usuario_recurso_rel',string="Implementador(es) del Cambio")
    aprobador_ids = fields.Many2many('res.users','gestor_usuario_implementador_rel',string="Aprobador(es) del Cambio")

    
    @api.onchange('fecha_inicio','fecha_cierre')
    def _verificar_fecha(self):
        if self.fecha_cierre < self.fecha_inicio:
            self.fecha_cierre= self.fecha_inicio
        if self.fecha_inicio > self.fecha_cierre:
            self.fecha_inicio = self.fecha_cierre
 
