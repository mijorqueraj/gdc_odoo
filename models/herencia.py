#-*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import date

class Herencia(models.Model):
    _inherit = 'gestor.orden'
    prioridades = fields.Selection([('alta','Alta'),('baja','Baja')])

    #se pueden ocupar las variables aplciadas en el modulo padre para verlas en el hijo
    def imprimir(self):
        print("hola soy heredado ",self.descripcion)

