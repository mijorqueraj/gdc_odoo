#-*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime

class Evaluacion(models.Model):
    _name='gestor.evaluacion'
    _description="modulo donde se evalua si el rfc cumple con las ccaracteristicas de cambio como tambien la evaluacion de las tareas aplicadas"

    estado_final =  fields.Selection([('completo','Terminado'),('medias','Incompleto'),('incompleto','No Realizado')],string="Estado")
    fecha = fields.Datetime(default=datetime.today(),string="Fecha de Evaluacion")
    consideraciones = fields.Text(string="Consideraciones")
    isAprobado = fields.Boolean(string="Aprobado")
    aprobado = fields.Char(string="Estado Final")
    
    orden_solicitud_id = fields.Many2one('gestor.orden',ondelete='cascade',string="Orden")
    
    @api.onchange('isAprobado')
    def change(self):
        if(self.isAprobado):
            self.aprobado = "Aprobado"
        else:
            self.aprobado="Reprobado"

    