#-*- coding: utf-8 -*-
from odoo import models, fields, api

class WizardNuevoRFC(models.TransientModel):
    _name = 'gestor.wizard_nuevo_rfc'
    fuente = fields.Char(required=True)
    obra_fijada = fields.Boolean()
    proveedor_fijado = fields.Boolean()


    @api.model
    def abrir_wizard(self, orden):
        instancia_wizard = self.create({
            'fuente': fuente,
            'obra_id': obra_id,
            'proveedor_id': proveedor_id,
            'obra_fijada': True if obra_id else False,
            'proveedor_fijado': True if proveedor_id else False,
            'lista_materiales_id': lista_materiales_id
            })
        return {
            'type': 'ir.actions.act_window',
            'name': titulo,
            'view_type': 'form',
            'view_mode': 'form',
            'res_id': instancia_wizard.id,
            'res_model': 'abastecimiento.wizard_nueva_compra',
            'views': [(False, 'form')],
            'domain': [],
            'context': self.env.context,
            'target': 'new',
            'nodestroy': False,
            'flags': {
                'form': {'action_buttons': False},
                }
            }