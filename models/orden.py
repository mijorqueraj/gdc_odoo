#-*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime
from dateutil.relativedelta import relativedelta

class Orden(models.Model):
    _name='gestor.orden'
    _description='Gestion de orden de solicitud'
    _order = "fecha desc"
    _rec_name = "razon_cambio"

    descripcion = fields.Text(string="Descripcion",required=True,deprecated=True)
    fecha = fields.Datetime(default=datetime.now(),string="Fecha Detección Falla o Inconveniente")
    origen = fields.Char(string="Departamento Origen", required=True)
    razon_cambio = fields.Selection([('cambio','Cambio'),('mantencion','Mantencion'),('actualizacion','Actualizacion'),('nuevo_servicio', 'Nuevos Servicios')],string='Tipo de Orden',default='cambio')
   
    recname="nombre"

    categoria = fields.Char(string="Categoria")
    sub_categoria = fields.Char(string="Sub Categoria" )

    # palabra reservada del sistema 
    state = fields.Selection([('borrador', 'Borrador'), ('por_validar', 'Por Validar'), ('cambio', 'Configuracion Cambio') ,('en_cierre','En Cierre'),('cierre','Cierre')], default='borrador')

    imagen = fields.Binary(string="Captura de Error",attachment=True)
    descripcion_help = fields.Text(string="Detalle de la Orden")

    #parte del rfc 
    prioridad = fields.Selection([('alta', 'Alta'),('normal','Normal'),('baja','Baja'),('urgente','Urgente')],default="normal",string="Prioriad")
    tipo_cambio  = fields.Selection([('estandar','Estándar'),('emergencia','Emergencia'),('simple','Simple')],default="estandar",string="Tipo de Cambio")
    fecha_inicio = fields.Datetime(default=datetime.now(),string="Inicio Trabajo")
    fecha_cierre = fields.Datetime(default=datetime.now(),string="Fin de Trabajo")
    tiempo_estimado = fields.Char(readonly=True,compute="_calcular_tiempo_estimado",string="Tiempo Estimado de Cambio")
    estado = fields.Selection([('pendiente','Pendiente'),('en_proceso','En Proceso'),('finalizado','Finalizado')],default="pendiente")

    diff_time = fields.Char(compute="_calcular_diferencia_tiempo")


    #Relaciones con tablas
    activo_id = fields.Many2one('gestor.activo',required=True, ondelete='cascade',string='Componente Afectado')
    recurso_ids = fields.Many2many('res.users', string="Personas Encargadas del cambio")
    respaldo_ids = fields.One2many('gestor.respaldos','orden_solicitud_id',string="Nueva Configuración")
    evaluacion_ids = fields.One2many('gestor.evaluacion','orden_solicitud_id',string="Evaluacion")
    
    archivos_ids = fields.Many2many('ir.attachment',string="Adjuntos")
 

    @api.onchange('razon_cambio')
    def descripcion_cambio(self):
        razon = self.razon_cambio
        if razon == 'cambio':
            self.descripcion_help = razon.capitalize() + " permite realizar el reemplazo del componente por otro, pero teniendo las mismas especificaciones que el anterior"
        if razon  == 'mantencion':
            self.descripcion_help = razon.capitalize() + " permite verificar detalles que puedan interrumpir el buen funcionamiento del componente, con el objetivo de corregir el probelma"
        if razon == 'actualizacion':
            self.descripcion_help = razon.capitalize() +  " permite verificar si el componente se encuentra en una version caducada para poder actualizar"
        if razon == 'nuevo_servicio':
            self.descripcion_help = razon.capitalize() + " permite realizar el reemplazo de un nuevo componente con otras especificaciones en comparacion con el anterior"

    def validar_orden(self):    

        if  not self.origen:
            return self.enviarNotificacion('Error','Algunos Campos estan incompletos','danger')
        else:
            self.state = 'por_validar'

    def validar_rfc(self):
        if self.tipo_cambio == 'simple':
            self.state = 'cierre'
        else:
            self.state = 'cambio'
        
    def rechazar_rfc(self):
        self.state= 'borrador'

    def realizar_cambio(self):
        if self.tipo_cambio == 'estandar':
            if not self.respaldo_ids:
                return self.enviarNotificacion('Agregar Configuración Nueva','Para los cambios de tipo estándar se requiere agregar una configuración nueva desde la pestaña NUEVA CONFIGURACION','warning')
            else:
                self.state='en_cierre'
        elif self.tipo_cambio == 'emergencia':
            self.state = 'cierre'

    def realizar_cierre(self):
        
        if self.estado != 'finalizado':
            return self.enviarNotificacion('Proyecto no Finalizado','Asegurese de que el campo ESTADO del proyecto en la pestaña IMPLEMENTADOR esté en finalizado','warning') 
        elif not self.evaluacion_ids:
            return self.enviarNotificacion('Evaluación Pendiente','Para los cambios estandar se requiere de una evaluación, por favor crear una en la pestaña de CIERRE DE CAMBIO','danger')
        else:
            self.state='cierre'
    
    def cancelar_todo(self):
        self.respaldo_ids.unlink()
        self.recurso_ids.unlink()
        self.evaluacion_ids.unlink()
        self.fecha_cierre = datetime.now()
        self.fecha_inicio = datetime.now()
        self.estado="pendiente"

        self.state = 'borrador'

    def enviarNotificacion(self,titulo,mensaje,tipo):
        notificacion = {
            'type':'ir.actions.client',
            'tag':'display_notification',
            'params':{
                'title':(titulo),
                'message':mensaje,
                'type':tipo,
                'sticky':False,
            },
        }
        return notificacion


    @api.onchange('fecha_inicio','fecha_cierre')
    def _verificar_fecha(self):
        if self.fecha_cierre < self.fecha_inicio:
            self.fecha_cierre = self.fecha_inicio
        if self.fecha_inicio > self.fecha_cierre:
            self.fecha_inicio = self.fecha_cierre

        if self.fecha_inicio < self.fecha:
            self.fecha_inicio = self.fecha
            return self.enviarNotificacion('Problema Fecha Inicio','No se puede comenzar el inicio del proyecto antes de la fecha de soliciutd','warning')
 

    #se puede acceder facilmente a los atributos de la referencia solo con la fk. se debe recorrer ya que se almacena en array
    def busqueda_tareas(self):
        for x in self.tarea_id:
            print(x.nombre)

    #calcula la diferencia del tiempo actual con la fecha de creacion
    @api.model
    def _calcular_diferencia_tiempo(self):
        try:
            if self.fecha:
                ahora = datetime.now()
                diff = relativedelta(ahora,self.fecha)

                if diff.years>0:
                    self.diff_time = "%d Mes con %d Dias %d Horas y %d Minutos"%(diff.months,diff.days,diff.hours,diff.minutes)
                if diff.months>0:
                    if diff.months == 1:
                        self.diff_time = "%d Mes con %d Dias %d Horas y %d Minutos"%(diff.months,diff.days,diff.minutes)
                    else:
                        self.diff_time = "%d Meses %d Días %d Horas %d Minutos"%(diff.months,diff.days,diff.hours,diff.minutes)

                elif diff.days > 0:
                    self.diff_time = "%d Días %d Horas %d Minutos"%(diff.days,diff.hours,diff.minutes)
                elif diff.hours > 0:
                    self.diff_time = "%d Horas %d Minutos"%(diff.hours,diff.minutes)
                else:
                    self.diff_time = "%d Minutos %d Segundos"%(diff.minutes,diff.seconds)
        except Exception as e:
            self.diff_time='Error'
            print("error :: ",e)

    @api.onchange('fecha_cierre')
    def _calcular_tiempo_estimado(self):
        try:
            if self.fecha_inicio and self.fecha_cierre:
                diff = relativedelta(self.fecha_cierre,self.fecha_inicio)

                if diff.years>0:
                    self.tiempo_estimado = "%d Mes con %d Dias %d Horas y %d Minutos"%(diff.months,diff.days,diff.hours,diff.minutes)
                if diff.months > 0:
                    if diff.months == 1:
                        self.tiempo_estimado = "%d Mes con %d Dias %d Horas y %d Minutos"%(diff.months,diff.days,diff.hours,diff.minutes)
                    else:
                        self.tiempo_estimado = "%d Meses con $d Dias %d Horas y %d Minutos"%(diff.months,diff.days,diff.hours,diff.minutes)
                elif diff.days>0:
                    self.tiempo_estimado = "%d Dias %d Horas %d Minutos"%(diff.days,diff.hours, diff.minutes)
                elif diff.hours>0:
                    self.tiempo_estimado = "%d Horas %d Minutos"%(diff.hours,diff.minutes)
                else:
                    self.tiempo_estimado="%d Minutos %d Segundos"%(diff.minutes,diff.seconds)
        except Exception as e:
            self.tiempo_estimado='Error'
            print("error :: ",e)
