#-*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime

class Tarea(models.Model):
    _name='gestor.tarea'
    _description = 'Modulo para la programacion de tareas que se haran en la solicitud de cambio, está directament asociada en el registro de un rfc, es la etapa final'
    
    nombre = fields.Char(string="Nombre de Tarea", required=True)
    descripcion = fields.Html(string="Descripcion de la tarea")
    estado = fields.Selection([('pendiente','Pendiente'),('en_proceso','En Proceso'),('terminado','Terminado')],default='pendiente')
    horas_estimadas = fields.Float(string="Horas de Trabajo Estimadas",required=True)
    impacto = fields.Selection([('muy_bajo', 'Muy Bajo'),('bajo','Bajo'),('moderado','Moderado'),('alto','Alto'),('muy_alto','Muy Alto')],required=True)
    detalle_impacto = fields.Text(string="Detalle del Impacto")

    #relaciones
    orden_solicitud_id = fields.Many2one('gestor.orden',ondelete="cascade", string="Orden")
    
    #atributos
    @api.onchange('impacto')
    def _informacion_impacto(self):
        if self.impacto == 'muy_bajo':
            self.detalle_impacto = 'Costo poco significante, el tiempo de implementacion es igual o menor que '+str(self.horas_estimadas) +'hrs. Pocos activos secundario afectados'
        if self.impacto == 'bajo':
            self.detalle_impacto = 'Costo aumenta un poco, el tiempo de implementacion puede aumentar a '+str(self.horas_estimadas*0.05 +  self.horas_estimadas) +'hrs. Varios activos secundarios afectados'
        if self.impacto == 'moderado':
            self.detalle_impacto = 'Costo aumenta entre un 10% a 20%, el tiempo de implementacion puede aumentar hasta  '+str(self.horas_estimadas*0.1+self.horas_estimadas) +'hrs. Algun activo principal afectado ligeramente'
        if self.impacto == 'alto':
            self.detalle_impacto = 'Costo aumenta hasta un 40%, el tiempo de implementacion puede aumentar hasta  '+str(self.horas_estimadas*0.2+self.horas_estimadas) +'hrs. Algun activo principal afectado sustancialmente'
        if self.impacto =='muy_alto':
            self.detalle_impacto = 'Costo aumenta más de 40%, el tiempo de implementación es mayor a  '+str(self.horas_estimadas*0.2+ self.horas_estimadas) +'hrs. Varios activos principales sustancialmente afectados'

    #funcion que permite ver atributo de modulos relacionados
    def ver_orden(self):        
        for i in self.orden_solicitud_id:
            print(i.prioridad)
