#-*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import date

class Activo(models.Model):
    _name="gestor.activo"
    _description = "Gestor de Activos"
    _rec_name ="nombre"
    
    nombre = fields.Char(string="Nombre",required=True)
    descripcion=fields.Text(string="Descripcion")
    tipo = fields.Selection([('software','Software'),('hardware','Hardware'),('otro','Otros')],'Tipo')
    imagen = fields.Binary(string="Imagen (opcional)")
